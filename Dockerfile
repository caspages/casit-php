FROM php:8.2.16-apache
ENV TZ=America/Los_Angeles
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN a2enmod rewrite
RUN a2enmod expires
COPY custom-config.ini /usr/local/etc/php/conf.d
RUN service apache2 restart
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libtidy-dev \
        libzip-dev \
        zip \
        libonig-dev \
    && docker-php-ext-install -j$(nproc) iconv pdo_mysql tidy \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd mysqli \
    && docker-php-ext-install zip
